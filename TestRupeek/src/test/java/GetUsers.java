import clients.CustomerSearchBl;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GetUsers extends CustomerSearchBl {
    CustomerSearchBl customerSearchBl=new CustomerSearchBl();
    CustomerSearchTest customerSearchTest=new CustomerSearchTest();

    @Test
    public void verifyDetails(){
        String token=customerSearchTest.getToken();
        Response getResponse= customerSearchBl.getUsers(token);
        String phoneNumber=getResponse.jsonPath().get("phone[0]");
        String name=getResponse.jsonPath().get("first_name[0]");
        Response getDetail=customerSearchBl.getUserFromPhone(token,phoneNumber);
        String nameGetDetail=getDetail.jsonPath().getString("first_name");
        customerSearchBl.validateStatusCode(getDetail,200);
        Assert.assertEquals(name,nameGetDetail);
    }

    @Test
    public void verifyDetailsWithWrongAuthentication(){
        Response getDetail=customerSearchBl.getUserFromPhone("12225451","985263258");
        customerSearchBl.validateStatusCode(getDetail,401);
    }

    @Test
    public void verifyDetailsWithWrongCustomerPhone(){
        String token=customerSearchTest.getToken();
        Response getResponse= customerSearchBl.getUsers(token);
        Response getDetail=customerSearchBl.getUserFromPhone(token,"25852147");
        customerSearchBl.validateStatusCode(getDetail,200);
    }
}
