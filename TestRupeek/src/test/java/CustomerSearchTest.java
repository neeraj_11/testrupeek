import clients.CustomerSearchBl;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CustomerSearchTest extends CustomerSearchBl {

    CustomerSearchBl customerSearch=new CustomerSearchBl();
    public String getToken()
    {
        Response response= customerSearch.createAuthentication();
        String token= response.jsonPath().getString("token");
        return token;
    }

    @Test
    public void verifyTokenApiWithWrongPayload(){
        Response response=customerSearch.createAuthenticationWithWrongPayload();
        customerSearch.validateStatusCode(response,401);
    }
}
