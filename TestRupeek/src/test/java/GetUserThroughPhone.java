import clients.CustomerSearchBl;
import io.restassured.response.Response;
import org.testng.annotations.Test;

public class GetUserThroughPhone extends CustomerSearchBl {
    CustomerSearchBl customerSearchBl=new CustomerSearchBl();
    CustomerSearchTest customerSearchTest=new CustomerSearchTest();
    @Test
    public void verifyDetailsOfUser(){
        String token=customerSearchTest.getToken();
        Response getResponse= customerSearchBl.getUsers(token);
        String phoneNumber=getResponse.jsonPath().get("phone[0]");
        Response getDetail=customerSearchBl.getUserFromPhone(token,phoneNumber);
    }
    @Test
    public void verifyDetailsOfUserWithWrongAuth(){
        Response getDetail=customerSearchBl.getUserFromPhone("52547852","9514533685");
        customerSearchBl.validateStatusCode(getDetail,401);
    }
}
