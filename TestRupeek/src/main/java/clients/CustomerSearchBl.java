package clients;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;

import java.util.HashMap;

public class CustomerSearchBl {

    public Response createAuthentication() {
        String url="http://13.126.80.194:8080/authenticate";
        HashMap<String,String> hmap=new HashMap<String, String>();
        hmap.put("username","rupeek");
        hmap.put("password","password");
        Response response = RestAssured.given().
                contentType(ContentType.JSON)
                .body(hmap).log().all()
                .post(url);
        System.out.println(response.asString());
        return response;
    }
    public Response createAuthenticationWithWrongPayload() {
        String url="http://13.126.80.194:8080/authenticate";
        HashMap<String,String> hmap=new HashMap<String, String>();
        hmap.put("username","Test");
        hmap.put("password","password");
        Response response = RestAssured.given().
                contentType(ContentType.JSON)
                .body(hmap).log().all()
                .post(url);
        System.out.println(response.asString());
        return response;
    }

    public Response getUsers(String header) {
        String url="http://13.126.80.194:8080/api/v1/users ";
        Response response = RestAssured.given().
                header("Authorization","Bearer "+header)
                .when().log().all()
                .get(url);
        System.out.println(response.asString());
        return response;
    }

    public Response getUserFromPhone(String header,String phoneNumber) {
        String url="http://13.126.80.194:8080/api/v1/users/{phone}";
        Response response = RestAssured.given().
                header("Authorization","Bearer "+header).
                pathParam("phone",phoneNumber)
                .when().log().all()
                .get(url);
        response.then().log().all();
        System.out.println(response.asString());
        return response;
    }

    public void validateStatusCode(Response response,int statusCode){
        Assert.assertEquals(response.getStatusCode(),statusCode);
    }
}
